
public class testcase {
	public static void main(String[] args){
		Library lib = new Library();
		Book book1 = new Book("A");
		Book book2 = new Book("B");
		Book book3 = new Book("C");
		Book book4 = new Book("D");
		S_Book book5 = new S_Book("SS");
		Stu_bachelor stu1 = new Stu_bachelor("Boom");
		Stu_master stu2 = new Stu_master("Win");
		Stu_doctor stu3 = new Stu_doctor("Hao");
		Teacher teach = new Teacher("John");
		Staff staff = new Staff("Jack");
		
		lib.addbook(book1);
		lib.addbook(book2);
		lib.addbook(book3);
		lib.addbook(book4);
		lib.addbook(book5);
		
		System.out.println(lib.borrow(stu1,book1));
		System.out.println(lib.borrow(stu2,book2));
		System.out.println(lib.borrow(stu3,book3));
		System.out.println(lib.borrow(teach,book4));
		System.out.println(lib.borrow(staff,book1));
		System.out.println(lib.borrow(stu1,book5));
		System.out.println(lib.borrow(stu2,book5));
		System.out.println(lib.borrow(stu3,book5));
		System.out.println(lib.borrow(teach,book5));
		System.out.println(lib.borrow(staff,book5));
		
	}
}
