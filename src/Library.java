import java.util.ArrayList;

public class Library {
	
	ArrayList<Book> book = new ArrayList<Book>();
	ArrayList<S_Book> S_book = new ArrayList<S_Book>();
	
	
	public void addbook (Book b){
		book.add(b);
	}
	public void addbook (S_Book b){
		S_book.add(b);
	}
	
	public void remove(Book b){
		book.remove(b);
	}
	
	public void remove(S_Book b){
		S_book.remove(b);
	}
	
	public String borrow(Stu_bachelor member, Book book){
		if(book.getstat().equals("Yes") && this.book.contains(book)){
			book.setstat("No");
			remove(book);
			return "DONE : "+member.getname()+" You can borrow around "+member.getdate()+" days";
			}
		else{
			return "NOT DONE : this book can't borrow";
		}
	}
	
	public String borrow(Stu_master member, Book book){
		if(book.getstat().equals("Yes") && this.book.contains(book)){
			book.setstat("No");
			remove(book);
			return "DONE : "+member.getname()+" You can borrow around "+member.getdate()+" days";
			}
		else{
			return "NOT DONE : this book can't borrow";
		}
	}
	public String borrow(Stu_doctor member, Book book){
		if(book.getstat().equals("Yes") && this.book.contains(book)){
			book.setstat("No");
			remove(book);
			return "DONE : "+member.getname()+" You can borrow around "+member.getdate()+" days";
			}
		else{
			return "NOT DONE : this book can't borrow";
		}
	}
	public String borrow(Teacher member, Book book){
		if(book.getstat().equals("Yes") && this.book.contains(book)){
			book.setstat("No");
			remove(book);
			return "DONE : "+member.getname()+" You can borrow around "+member.getdate()+" days";
			}
		else{
			return "NOT DONE : this book can't borrow";
		}
	}
	public String borrow(Staff member, Book book){
		if(book.getstat().equals("Yes") && this.book.contains(book)){
			book.setstat("No");
			remove(book);
			return "DONE : "+member.getname()+" You can borrow around "+member.getdate()+" days";
			}
		else{
			return "NOT DONE : this book can't borrow";
		}
	}
	
	public String borrow(Stu_bachelor member, S_Book book){
		return "NOT DONE : this book can't borrow";
	}
	public String borrow(Stu_master member, S_Book book){
		return "NOT DONE : this book can't borrow";
	}
	public String borrow(Stu_doctor member, S_Book book){
		return "NOT DONE : this book can't borrow";
	}
	public String borrow(Teacher member, S_Book book){
		return "NOT DONE : this book can't borrow";
	}
	public String borrow(Staff member, S_Book book){
		return "NOT DONE : this book can't borrow";
	}
	
	public void returnbook(Stu_bachelor member,Book book){
		addbook(book);
		book.setstat("Yes");
	}
	public void returnbook(Stu_master member,Book book){
		addbook(book);
		book.setstat("Yes");
	}
	public void returnbook(Stu_doctor member,Book book){
		addbook(book);
		book.setstat("Yes");
	}
	public void returnbook(Teacher member,Book book){
		addbook(book);
		book.setstat("Yes");
	}
	public void returnbook(Staff member,Book book){
		addbook(book);
		book.setstat("Yes");
	}
	
	
	
	
}
